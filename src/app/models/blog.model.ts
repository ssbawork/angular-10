import { Image } from "./image.model";

export class Blog {
  id: number;
  category: null;
  image: Image;
  name: string;
  view: number;
  lang: string;
  country: string;
  score: number;
  data: string;
  type: string;
  article_type: string;
  created_at: Date | undefined;
  updated_at: Date | undefined;
}