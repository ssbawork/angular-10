export class Image {
  id: number;
  path: string;
  name: string;
  alt: string;
  main: boolean;
  created_at: Date | undefined;
  updated_at: Date | undefined;
}