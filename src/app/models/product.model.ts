import { Category } from "./category.model";
import { Image } from "./image.model";
import { Brand } from "./brand.model";

export class Product {
  id: number;
  brand: Brand;
  name: string;
  type: string;
  tar: number;
  nicotine: number;
  color: string;
  price: number;
  views: number;
  featured: boolean;
  is_favourite: boolean;
  discount: number;
  rate: number;
  images: Image[];
  category: Category;
  created_at: Date | undefined;
  updated_at: Date | undefined;
}