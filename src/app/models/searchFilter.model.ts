export enum SFRegionType {
    Type,
    Price,
    Brands,
    Tough,
    Сolor,
}

export class SearchFilter {
    id: number;
    region: SFRegionType;
    code: string;
    name: string;
}