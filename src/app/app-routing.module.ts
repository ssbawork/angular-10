import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './parts/login/login.component';
import { ShopComponent } from './parts/shop/shop.component';
import { ProductComponent } from './parts/product/product.component';
import { CartComponent } from './parts/cart/cart.component';
import { FaqComponent } from './parts/faq/faq.component';
import { BulkComponent } from './parts/bulk/bulk.component';
import { DeliveryComponent } from './parts/delivery/delivery.component';
import { ContactsComponent } from './parts/contacts/contacts.component';
import { WhoWeAreComponent } from './parts/who-we-are/who-we-are.component';
import { OrderStatusComponent } from './parts/order-status/order-status.component';
import { ThankYouComponent } from './parts/thank-you/thank-you.component';
import { NotFoundComponent } from './parts/not-found/not-found.component';
import { HomeComponent } from './parts/home/home.component';
import { OrderConfirmationComponent } from './parts/order-confirmation/order-confirmation.component';
import { BlogsComponent } from './parts/blogs/blogs.component';
import { BlogArticleComponent } from './parts/blog-article/blog-article.component';

const routes: Routes = [
  {
    path: 'profile',
    loadChildren: () => import('./parts/profile/profile.module').then(m => m.ProfileModule),
  },
  {
    path: 'categories',
    loadChildren: () => import('./parts/categories/categories.module').then(m => m.CategoriesModule),
  },
  {path: 'blogs', component: BlogsComponent},
  {path: 'blogs/:articleId', component: BlogArticleComponent},
  {path: 'login', component: LoginComponent},
  {path: 'shop', component: ShopComponent},
  {path: 'cart', component: CartComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'bulk', component: BulkComponent},
  {path: 'delivery', component: DeliveryComponent},
  {path: 'contactus', component: ContactsComponent},
  {path: 'who-we-are', component: WhoWeAreComponent},
  {path: 'orders/:orderid', component: OrderStatusComponent},
  {path: 'orders/:orderid/confirmed', component: OrderConfirmationComponent},
  {path: 'orders/:orderid/thanks', component: ThankYouComponent},
  {path: ':cat/:code', component: ProductComponent},
  {path: '', component: HomeComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }