import { EventEmitter, Inject, Injectable, Output, PLATFORM_ID } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import * as moment from "moment";
import { User } from "../models/user.model";
import { tap } from "rxjs/operators";
import { LocalStorageService } from './ssr/localStorage.service';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isBrowser: boolean;
  private _changeLoginState = new BehaviorSubject<any>(null);
  authState = this._changeLoginState.asObservable();
  
  constructor(private http: HttpClient, private localStorageService: LocalStorageService,  @Inject(PLATFORM_ID) platformId: Object) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  login(email:string, password:string, r:boolean = false ) {
    return this.http.post<User>('/user/login', {email, password, r}).pipe(
      tap(res => {
        this.setSession(res);
        this._changeLoginState.next(true);
      })
    );
  }

  registr(email:string, password:string) {
    return this.http.post<User>('/user/registration', {email, password}).pipe(
      tap(res => {
        this.setSession(res);
        this._changeLoginState.next(true);
      })
    );
  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expires_in,'second');
    this.localStorageService.setItem('access_token', authResult.access_token);
    this.localStorageService.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
  }

  logout() {
    this.localStorageService.removeItem("access_token");
    this.localStorageService.removeItem("expires_at");
    this._changeLoginState.next(false);
  }

  isLoggedIn() {

    if(this.isBrowser == false){    
      return false;
    }

    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = this.localStorageService.getItem("expires_at");
    
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}