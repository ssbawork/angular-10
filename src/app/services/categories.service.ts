import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Pagination } from '../models/pagination.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) {}

  fetchProducts(page :number = 0): Observable<Pagination> {

    let params = new HttpParams();

    page ++;
    params = params.set("page",page.toString());

    
    return this.http.get<Pagination>(`/c/1`,{
      params : params
    });
  }


  

}