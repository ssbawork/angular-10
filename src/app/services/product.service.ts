import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  getProductDetails(id :number): Observable<Product> {
    return this.http.get<Product>(`/product/details/` + id);
  }

  getFeaturedProducts(count :number): Observable<Product[]> {
    let params = new HttpParams();
    params = params.set("count",count.toString());    
    
    return this.http.get<Product[]>(`/products/featured`,{
      params : params
    });
  }
  
  getRecommendProducts(count :number): Observable<Product[]> {
    let params = new HttpParams();
    params = params.set("count",count.toString());    
    
    return this.http.get<Product[]>(`/products/recommends`,{
      params : params
    });
  }
  
  getBestSellingProducts(count :number): Observable<Product[]> {
    let params = new HttpParams();
    params = params.set("count",count.toString());    
    
    return this.http.get<Product[]>(`/products/bestselling`,{
      params : params
    });
  }
}
