import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { SearchFilter } from '../models/searchFilter.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchFilterService {

  constructor(private http: HttpClient) {}
 
  getFilters(): Observable<SearchFilter[]> {

    return this.http.get<SearchFilter[]>(`/sf/`);
  }


  
}
