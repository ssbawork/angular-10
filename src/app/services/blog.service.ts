import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Blog } from '../models/blog.model';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) {}

  
  getTips(count :number): Observable<Blog[]> {
    let params = new HttpParams();
    params = params.set("count",count.toString());    
    
    return this.http.get<Blog[]>(`/blog/tips`,{
      params : params
    });
  }


}