import { Injectable } from '@angular/core';
import { AppComponent } from '../../app.component';

class Document implements Document{
  body: HTMLElement;
  documentElement: HTMLElement | null;
  scrollingElement: Element | null;
}

@Injectable()
export class DocumentService {

  public ref: Document;

  constructor() {
    this.ref = new Document();

    AppComponent.isBrowser.subscribe(isBrowser => {
      if (isBrowser) {
        this.ref = document;
      }
    });
  }

}