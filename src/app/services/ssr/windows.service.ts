import { Injectable } from '@angular/core';
import { AppComponent } from '../../app.component';

class Window {
  screenX: number;
  screenY: number;
  scrollX: number;
  scrollY: number;
  innerHeight: number;
  innerWidth: number;
  onscroll: (this: GlobalEventHandlers, ev: Event) => any;
  scroll: (options?: ScrollToOptions) => void;
}

@Injectable()
export class WindowService {

  public ref: Window;

  constructor() {
    this.ref = new Window();

    AppComponent.isBrowser.subscribe(isBrowser => {
      if (isBrowser) {
        this.ref = window;
      }
    });
  }

}