import { Injectable } from '@angular/core';
import { WindowService } from './ssr/windows.service';

@Injectable({
  providedIn: 'root'
})
export class MobileService {

  constructor(private _window: WindowService) { }


  isMobile(): boolean {

    if (this._window.ref.innerWidth <= 768) {
      return true;
    }
    return false;
  }

  
  isSmallMobile(): boolean {

    if (this._window.ref.innerWidth <= 440) {
      return true;
    }
    return false;
  }
}

