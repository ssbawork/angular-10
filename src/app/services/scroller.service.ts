import { Injectable } from '@angular/core';
import { WindowService } from './ssr/windows.service';
import { DocumentService } from './ssr/document.service';

@Injectable({
  providedIn: 'root'
})
export class ScrollerService {

  private scrolledFromTop = false;
  private readonly isBrowser: boolean = typeof window !== 'undefined';

  constructor(private _window: WindowService, private _document: DocumentService) { }



  public onWindowScroll(): boolean {
    let show = false;
    const position: number = this._document.ref.documentElement?.scrollTop || this._document.ref.scrollingElement?.scrollTop;

    if (position === 0) {
      show = false;
      this.scrolledFromTop = false;
    }else{
      show = true;
      this.scrolledFromTop = true;

    }
  
    return show;
  }
/*
  private classicMode(position: number): boolean {
    let show = false;
    if (this.isBrowser && position > window.innerHeight) {
      show = true;
    } else {
      show = false;
    }
    return show;
  }

  private smartMode(position: number): boolean {
    let show = false;

    return show;
  }
*/
  public scrollToTop(): void {
    //this._window.ref.scroll({ top: 0, left: 0, behavior: 'smooth' });

    if (this.isBrowser) {
      window.scroll({ top: 0, left: 0, behavior: 'smooth' });
    }

  }



}
