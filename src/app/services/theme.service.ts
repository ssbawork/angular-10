import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Theme } from "../themes/theme.interface";
import { White } from "../themes/white.theme";
import { Dark } from "../themes/Dark.theme";
import { DocumentService } from './ssr/document.service';
import { LocalStorageService } from './ssr/localStorage.service';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private active: Theme = White;
  private availableThemes: Theme[] = [White, Dark];

  constructor(private _document: DocumentService, private localStorageService: LocalStorageService) { 
    
  }

  getAvailableThemes(): Theme[] {
    return this.availableThemes;
  }

  getActiveTheme(): Theme {
    return this.active;
  }

  getActiveThemeName(): string {
    return this.active.name;
  }

  isDarkTheme(): boolean {
    return this.active.name === Dark.name;
  }

  isWhiteTheme(): boolean {
    return this.active.name === White.name;
  }

  setDarkTheme(): void {
    this.setActiveTheme(Dark);
  }

  setWhiteTheme(): void {
    this.setActiveTheme(White);
  }

  toggleTheme(): void {
    if( this.isDarkTheme() == true ){
      this.setWhiteTheme();
    }else{
      this.setDarkTheme();
    }
  }

  setActiveTheme(theme: Theme): void {
    this.active = theme;

    this.localStorageService.setItem("theme", theme.name);

    Object.keys(this.active.properties).forEach(property => {     
      if(this._document.ref.documentElement != null){
        this._document.ref.documentElement.style.setProperty(
          property,
          this.active.properties[property]
        );
      }
    });
  }

  initTheme(): void {
    let themeName = this.localStorageService.getItem("theme");

    let theme: Theme = this.availableThemes.find((x) => {
      return x.name == themeName
    });

    if(theme != null){
      this.setActiveTheme(theme);
    }else{
      this.setWhiteTheme();
    }

  }


/*
  public url: string = null;

  current: string = "white";
  availble: string[] = ["white","Dark"];

  constructor(@Inject(DOCUMENT) private document: Document) { 
    //TODO SAVE LOAD FROM LOCALSTORAGE
  }

  private changeThemeUrl() {
    for (let i = 0; i < this.availble.length; i++) {
      let name = this.availble[i];
      if(name != this.current){
        this.current = name;
        this.url = '/assets/t/'+ name +'.css';
        return this.url;
      }
    }
  }

  private createLink() {
    let link: HTMLLinkElement = this.document.createElement('link');
    link.setAttribute('type', 'text/css');
    link.setAttribute('id', 'theme');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', this.url);

    let linkDom = this.document.querySelector("#theme");
    if(linkDom != null){
      this.document.head.removeChild( linkDom );
    }

    this.document.head.appendChild(link);
  }
  

  change(){
    this.changeThemeUrl();
    this.createLink();
  }*/
}
