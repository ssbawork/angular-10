import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent implements OnInit {

  reviewForm:FormGroup;

  constructor(private fb:FormBuilder) { 
    this.reviewForm = this.fb.group({
      feelings: ['',Validators.required],
      advantages: ['',Validators.required],
      disadvantages: ['',Validators.required],
    });  
  }

  ngOnInit(): void {
  }

}
