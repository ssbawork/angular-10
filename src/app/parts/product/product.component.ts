import { animate, group, query, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

const left = [
  query(':enter, :leave', style({}), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(-200px)' }), animate('.3s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.3s ease-out', style({ transform: 'translateX(200px)' }))], {
      optional: true,
    }),
  ]),
];

const right = [
  query(':enter, :leave', style({}), { optional: true }),
  group([
    query(':enter', [style({ transform: 'translateX(200px)' }), animate('.3s ease-out', style({ transform: 'translateX(0%)' }))], {
      optional: true,
    }),
    query(':leave', [style({ transform: 'translateX(0%)' }), animate('.3s ease-out', style({ transform: 'translateX(-200px)' }))], {
      optional: true,
    }),
  ]),
];

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  animations: [
    trigger('animateImageSlider', [
      transition(':increment', right),
      transition(':decrement', left),
    ]),
  ]
})
export class ProductComponent implements OnInit {

  productCode: string = null;
  product: Product = null;
  isLoaded: boolean = false;
  imageCounter: number = 2;
  currentTab = 'comment';

  // TODO Imager Create imgae url array from SERV
  images = [
    '/assets/content/slider_1.jpg',
    '/assets/content/slider_2.jpg',
    '/assets/content/content_3.jpg',
    '/assets/content/slider_3.jpg'
  ];

  constructor(private route: ActivatedRoute,  private productService: ProductService) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.productCode = params['code'];

      if(this.isLoaded == false){
        this.productService.getProductDetails(1).subscribe(res => {
          this.product = res;
          this.isLoaded = true;
        });
      }
    });
  }

  showTab(name:  string){
    this.currentTab = name;
  }

  changeImage($event){
    let touch = $event.changedTouches[0];
    let isLeft = touch.pageX > $event.srcElement.offsetLeft + ($event.srcElement.offsetWidth / 2);

    if(isLeft){
      if(this.imageCounter > 0){
        this.imageCounter--;
      }
    }else{
      if(this.imageCounter < 3){
        this.imageCounter++;
      }
    }
  }

}
