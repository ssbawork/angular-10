import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductQAComponent } from './product-qa.component';

describe('ProductQAComponent', () => {
  let component: ProductQAComponent;
  let fixture: ComponentFixture<ProductQAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductQAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductQAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
