import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'product-qa',
  templateUrl: './product-qa.component.html',
  styleUrls: ['./product-qa.component.scss']
})
export class ProductQAComponent implements OnInit {

  qForm:FormGroup;

  constructor(private fb:FormBuilder) { 
    this.qForm = this.fb.group({
      questionArea: ['',Validators.required]
    });  
  }

  ngOnInit(): void {
  }

}
