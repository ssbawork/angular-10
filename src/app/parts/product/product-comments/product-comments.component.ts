import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'product-comments',
  templateUrl: './product-comments.component.html',
  styleUrls: ['./product-comments.component.scss']
})
export class ProductCommentsComponent implements OnInit {

  commentForm:FormGroup;

  constructor(private fb:FormBuilder) { 
    this.commentForm = this.fb.group({
      commentArea: ['',Validators.required]
    });  
  }

  ngOnInit(): void {
  }

}
