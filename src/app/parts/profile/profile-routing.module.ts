import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ProfileAddressesComponent } from './profile-addresses/profile-addresses.component';
import { ProfileFavouritesComponent } from './profile-favourites/profile-favourites.component';
import { ProfileHistoryComponent } from './profile-history/profile-history.component';
import { ProfileOrdersComponent } from './profile-orders/profile-orders.component';
import { ProfileOrderComponent } from './profile-order/profile-order.component';

const routes: Routes = [


  { path: '', component: ProfileComponent,
  children: [
    { path: '', component: ProfileMainComponent },
    { path: 'addresses', component: ProfileAddressesComponent },
    { path: 'favourites', component: ProfileFavouritesComponent },
    { path: 'history', component: ProfileHistoryComponent },
    { path: 'orders', component: ProfileOrdersComponent },
    { path: 'order/:orderid', component: ProfileOrderComponent }
  ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }