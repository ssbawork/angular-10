import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { ProfileAddressesComponent } from './profile-addresses/profile-addresses.component';
import { ProfileFavouritesComponent } from './profile-favourites/profile-favourites.component';
import { ProfileHistoryComponent } from './profile-history/profile-history.component';
import { ProfileOrdersComponent } from './profile-orders/profile-orders.component';
import { ProfileMainComponent } from './profile-main/profile-main.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileOrderComponent } from './profile-order/profile-order.component';


@NgModule({
  declarations: [ProfileComponent, ProfileAddressesComponent, ProfileFavouritesComponent, ProfileHistoryComponent, ProfileOrdersComponent, ProfileMainComponent, ProfileOrderComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule 
  ]
})
export class ProfileModule { }
