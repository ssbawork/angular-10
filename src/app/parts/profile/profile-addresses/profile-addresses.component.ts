import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-addresses',
  templateUrl: './profile-addresses.component.html',
  styleUrls: ['./profile-addresses.component.scss']
})
export class ProfileAddressesComponent implements OnInit {

  addressForm:FormGroup;

  constructor(private fb:FormBuilder) { 
    this.addressForm = this.fb.group({
      FristName: ['',Validators.required],
      LastName: ['',Validators.required],
      Email: ['',Validators.required],
      Phone: ['',Validators.required],
      Region: ['',Validators.required],
      City: ['',Validators.required],
      Address1: ['',Validators.required],
      Zip: ['',Validators.required]
    });
  }

  ngOnInit(): void {
  }

}
