import { Component, OnInit } from '@angular/core';
import { MobileService } from 'src/app/services/mobile.service';

@Component({
  selector: 'main-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public mobile: boolean = false;

  constructor(private _mobile: MobileService) { }

  ngOnInit(): void {
    this.mobile = this._mobile.isMobile();
  }

}
