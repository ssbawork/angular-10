import { Component, HostListener, OnInit } from '@angular/core';
import { ScrollerService } from '../../services/scroller.service';

@Component({
  selector: 'go-up',
  templateUrl: './go-up.component.html',
  styleUrls: ['./go-up.component.scss']
})
export class GoUpComponent implements OnInit {

  public show = false;

  constructor(private scroller: ScrollerService) {}

  ngOnInit(): void {  }


  @HostListener('window:scroll')
  public onWindowScroll() {
    this.show = this.scroller.onWindowScroll();
  }
  public scrollToTop(): void {
    this.scroller.scrollToTop();
  }


}
