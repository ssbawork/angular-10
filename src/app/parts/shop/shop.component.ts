import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { MobileService } from 'src/app/services/mobile.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
  animations: [
    trigger('SidebarAnimation', [
      state('on', style({ transform: 'translateY(0)' })),
      state('off', style({ transform: 'translateY(-200%)' })),
      transition('off => on', [
        animate(300, style({ transform: 'translateY(0)' }))
      ]),
      transition('on => off', [
        animate(300, style({ transform: 'translateY(-200%)' }))
      ]),
    ])
  ]
})
export class ShopComponent implements OnInit {

  sidebarStatus = "on";

  constructor(private _mobile: MobileService) { }

  ngOnInit(): void {
    if(this._mobile.isMobile() == true){
      this.sidebarStatus = "off";
    }else{
      this.sidebarStatus = "on";
    }
  }

  toggleSidebar() {
    if(this.sidebarStatus == "off"){
      this.sidebarStatus = "on";
    }else{
      this.sidebarStatus = "off";
    }
  }



}
