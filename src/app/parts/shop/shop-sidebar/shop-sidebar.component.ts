import { Options } from '@angular-slider/ngx-slider';
import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { SearchFilter, SFRegionType } from "../../../models/searchFilter.model";
import { SearchFilterService } from "../../../services/searchFilter.service";

@Component({
  selector: 'shop-sidebar',
  templateUrl: './shop-sidebar.component.html',
  styleUrls: ['./shop-sidebar.component.scss']
})
export class ShopSidebarComponent implements OnInit {

  SearchForm:FormGroup;

  private allFilters: SearchFilter[];

  TypeFilters:SearchFilter[] = [];
  PriceFilters:SearchFilter[] = [];
  BrandsFilters:SearchFilter[] = [];
  ToughFilters:SearchFilter[] = [];
  СolorFilters:SearchFilter[] = [];

  isBrowser: boolean;

  value: number = 10;
  highValue: number = 90;
  options: Options = {
    floor: 0,
    ceil: 100,
    step: 0.1,
    showTicks: false
  };

  constructor(private fb:FormBuilder, private sf:SearchFilterService, @Inject(PLATFORM_ID) private platformId) {
    this.SearchForm = this.fb.group({});
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
  
  }


}
