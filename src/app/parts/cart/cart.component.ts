import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  CartForm:FormGroup;

  

  constructor(private fb:FormBuilder, private router: Router) { 
    
    this.CartForm = this.fb.group({
      firstName: ['',Validators.required],
      lastName: ['',Validators.required],
      email: ['',Validators.required],
      phone: ['',Validators.required],
      address: ['',Validators.required],
      region: ['',Validators.required],
      city: ['',Validators.required],
      postalcode: ['',Validators.required]
    });
    
  }

  ngOnInit(): void {
  }

}
