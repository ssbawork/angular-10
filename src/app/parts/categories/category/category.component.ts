import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/product.model';
import { Pagination } from '../../../models/pagination.model';
import { WindowService } from '../../../services/ssr/windows.service';
import { DocumentService } from '../../../services/ssr/document.service';
import { CategoriesService } from '../../../services/categories.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  constructor(private _window: WindowService, private _document: DocumentService, private categoriesService: CategoriesService) { }

  products: Product[] = [];
  pagination: Pagination;
  isLoading = false;
  loadedAll = false;
  isFirstLoad = true;

  ngOnInit(): void {
    this.getProducts();
    this.handleScroll();
  }

  handleScroll(): void {
    this._window.ref.onscroll = () => this.detectBottom();
  }

  getProducts(page: number = 0): void {
    this.isLoading = true;
    this.categoriesService.fetchProducts(page).subscribe(res => {
        if (res.data.length) {
          this.pagination = res;
          this.products.push(...res.data);
        } else {
          this.loadedAll = true;
        }
        this.isLoading = false;
        this.isFirstLoad = false;
    });
  }

  detectBottom(): void {
      if ((  this._window.ref.innerHeight + this._window.ref.scrollY ) >= this._document.ref.body.offsetHeight ) {
        if (!this.loadedAll) {
          this.getProducts(this.pagination ? this.pagination.current_page : null);
        }
      }
  }
}
