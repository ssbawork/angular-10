import { Component, HostListener, OnInit, SimpleChanges } from '@angular/core';
import { ThemeService } from 'src/app/services/theme.service';
import { state, trigger, transition, style, animate, query, group, animateChild } from '@angular/animations';
import { MobileService } from 'src/app/services/mobile.service';
import { filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/ssr/localStorage.service';

@Component({
  selector: 'header-bottom',
  templateUrl: './header-bottom.component.html',
  styleUrls: ['./header-bottom.component.scss'],
  animations: [
    trigger('burgerTopAnimation', [
      state('on', style({
        transform: 'rotate(45deg)',
        top: '{{top}}'
      }), {params: {top: '5px'}}),
      state('off', style({
        transform: 'rotate(0deg)',
        top: '0px'
      })),
      transition('* => *', [
        animate('0.2s')
      ]),
    ]),
    trigger('burgerBottomAnimation', [
      state('on', style({
        transform: 'rotate(-45deg)',
        bottom: '{{bottom}}'
      }), {params: {bottom: '4px'}}),
      state('off', style({
        transform: 'rotate(0deg)',
        bottom: '0px'
      })),
      transition('* => *', [
        animate('0.2s')
      ]),
    ]),
    trigger('burgerMenuAnimation', [
      state('on', style({ transform: 'translateY(0)' })),
      state('off', style({ transform: 'translateY(-100%)' })),
      transition('off => on', [
        animate(300, style({ transform: 'translateY(0)' }))
      ]),
      transition('on => off', [
        animate(300, style({ transform: 'translateY(-100%)' }))
      ]),
    ])
  ]
})

export class HeaderBottomComponent implements OnInit {

  burgerStatus = "off";
  burgerTopAnimation_Top = "5px";
  burgerBottomAnimation_Bottom = "4px";

  public isLoggon;

  public icons = {
    "cart": {
      "White": "/assets/i/cart_dark.svg",
      "Dark": "/assets/i/cart_white.svg"
    },
    "bitcoin": {
      "White": "/assets/i/bitcoin_dark.svg",
      "Dark": "/assets/i/bitcoin_white.svg"
    },
    "heart": {
      "White": "/assets/i/heart_dark.svg",
      "Dark": "/assets/i/heart_white.svg"
    },
    "search": {
      "White": "/assets/i/search_dark.svg",
      "Dark": "/assets/i/search_white.svg"
    },
  };

  constructor(public themeService: ThemeService, private _mobile: MobileService, private router: Router, private authService: AuthService, private localStorageService: LocalStorageService) {

  }

  ngOnInit(): void {
    this.fixAnimation();
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))  
    .subscribe((event: NavigationEnd) => {
      this.burgerStatus = "off";
    });

    const expiration = this.localStorageService.getItem("expires_at");
    this.isLoggon = expiration == null;
  }

  ngAfterContentChecked(): void{
    this.authService.authState.subscribe(state => {
      this.isLoggon = state;
    });
  }

  changeTheme() { 
    this.themeService.toggleTheme();
  }

  toggleBurger() {
    this.fixAnimation();
    if(this.burgerStatus == "on"){
      this.burgerStatus = "off";
    } else {
      this.burgerStatus = "on";
    }
  }

  fixAnimation() {
    if(this._mobile.isSmallMobile() == true){
      this.burgerTopAnimation_Top = "2px";
      this.burgerBottomAnimation_Bottom = "3px";
    }else{
      this.burgerTopAnimation_Top = "5px";
      this.burgerBottomAnimation_Bottom = "4px";
    }
  }

  @HostListener('window:orientationchange', ['$event'])
  onOrientationChange(event) {
    this.burgerStatus = "off";
  }

  logout(){
    
    this.authService.logout();
    this.router.navigateByUrl('/');
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  isLoggedOut() {
    return this.authService.isLoggedOut();
  }

  animationStarted(event){
    /*
    console.log(event);

    console.log(`Animation Trigger: ${event.triggerName}`);

    // phaseName is start or done
    console.log(`Phase: ${event.phaseName}`);

    // in our example, totalTime is 1000 or 1 second
    console.log(`Total time: ${event.totalTime}`);

    // in our example, fromState is either open or closed
    console.log(`From: ${event.fromState}`);

    // in our example, toState either open or closed
    console.log(`To: ${event.toState}`);

    // the HTML element itself, the button in this case
    console.log(`Element: ${event.element}`);
    */
  }
}