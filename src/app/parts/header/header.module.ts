import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { HeaderTopComponent } from './header-top/header-top.component';
import { HeaderBottomComponent } from './header-bottom/header-bottom.component';
import { RouterModule } from '@angular/router';
import { HeaderSearchComponent } from './header-search/header-search.component';

@NgModule({
  declarations: [
    HeaderComponent, 
    HeaderTopComponent,
    HeaderBottomComponent,
    HeaderSearchComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    HeaderComponent
  ]
})
export class HeaderModule { }
