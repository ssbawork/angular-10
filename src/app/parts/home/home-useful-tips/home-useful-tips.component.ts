import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Blog } from 'src/app/models/Blog.model';
import { BlogService } from 'src/app/services/blog.service';

@Component({
  selector: 'home-useful-tips',
  templateUrl: './home-useful-tips.component.html',
  styleUrls: ['./home-useful-tips.component.scss']
})
export class HomeUsefulTipsComponent implements OnInit {

  allBlogs: Blog[];
  
  count: number = 4;
  loadingArray: number[] = [];

  isBrowser: boolean;
  
  constructor( @Inject(PLATFORM_ID) platformId: Object, private blogService: BlogService) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.loadingArray = Array(this.count).fill(1);
    if(this.isBrowser){
      this.blogService.getTips(this.count).subscribe(res => {
        this.allBlogs = res;
        this.loadingArray = [];
      });
    }
  }
  

}
