import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeUsefulTipsComponent } from './home-useful-tips.component';

describe('HomeUsefulTipsComponent', () => {
  let component: HomeUsefulTipsComponent;
  let fixture: ComponentFixture<HomeUsefulTipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeUsefulTipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeUsefulTipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
