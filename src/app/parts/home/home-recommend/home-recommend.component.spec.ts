import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRecommendComponent } from './home-recommend.component';

describe('HomeRecommendComponent', () => {
  let component: HomeRecommendComponent;
  let fixture: ComponentFixture<HomeRecommendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeRecommendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRecommendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
