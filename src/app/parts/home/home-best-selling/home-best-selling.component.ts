import { Component, Inject, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'home-best-selling',
  templateUrl: './home-best-selling.component.html',
  styleUrls: ['./home-best-selling.component.scss']
})
export class HomeBestSellingComponent implements OnInit {

 
  allProducts: Product[];
  
  count: number = 4;
  loadingArray: number[] = [];

  isBrowser: boolean;
  
  constructor( @Inject(PLATFORM_ID) platformId: Object, private productService: ProductService) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.loadingArray = Array(this.count).fill(1);
    if(this.isBrowser){
      this.productService.getBestSellingProducts(this.count).subscribe(res => {
        console.log(res);
        this.allProducts = res;
        this.loadingArray = [];
      });
    }
  }

  getMainImageSrc(product: Product): string {
    let image = product.images.filter(x => x.main == true);

    if(image.length < 1){
      return "";
    }
    
    return "/assets/products/" + image[0].name;
  }

  addtoCart(product: Product): void{


    console.log(product);
  }
}
