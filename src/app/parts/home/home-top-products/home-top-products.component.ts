import { Component, Inject, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';
import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'home-top-products',
  templateUrl: './home-top-products.component.html',
  styleUrls: ['./home-top-products.component.scss']
})
export class HomeTopProductsComponent implements OnInit {

  allProducts: Product[];
  currentProducts: Product[];
  
  count: number = 8;
  loadingArray: number[] = [];

  currentTab: string = 'cigarettes';
  isBrowser: boolean;
  
  constructor( @Inject(PLATFORM_ID) platformId: Object, private productService: ProductService) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  ngOnInit(): void {
    this.loadingArray = Array(this.count).fill(1);
    if(this.isBrowser){
      this.productService.getFeaturedProducts(this.count).subscribe(res => {
        this.allProducts = res;
        this.loadingArray = [];
        this.currentProducts = this.allProducts.filter(x => x.type == this.currentTab);
      });
    }
  }

  getMainImageSrc(product: Product): string {
    let image = product.images.filter(x => x.main == true);

    if(image.length < 1){
      return "";
    }
    
    return "/assets/products/" + image[0].name;
  }

  changeCurrentProducts(type: string): void {
    if(this.allProducts && this.allProducts.length > 0){
      this.currentProducts = this.allProducts.filter(x => x.type == type);
      this.currentTab = type;
    }
  }

  addtoCart(product: Product): void{


    console.log(product);
  }
}
