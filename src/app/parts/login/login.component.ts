import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  registerForm:FormGroup;

  constructor(private fb:FormBuilder, private authService: AuthService, private router: Router, private toastr: ToastrService, private logger: NGXLogger, private translocoService: TranslocoService) {

    if(this.authService.isLoggedIn()){
      this.router.navigateByUrl('/');
    }
    
    this.loginForm = this.fb.group({
      loginEmail: ['',[Validators.required, Validators.email]],
      loginPassword: ['',[Validators.required, Validators.minLength(6)]]
    });

    this.registerForm = this.fb.group({
      registerEmail: ['',Validators.required],
      registerPassword: ['',[Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit(): void {
  }

  login(): void { 
    const loginValues = this.loginForm.value;
    console.log(loginValues);
    if (loginValues.loginEmail && loginValues.loginPassword) {
        this.authService.login(loginValues.loginEmail, loginValues.loginPassword).subscribe(
                result => {
                  this.logger.debug("User successfully Logged In with Email: " + loginValues.loginEmail + " Password: " + loginValues.loginPassword);
                  this.router.navigateByUrl('/');
                },
                error => {
                  let error_i18l_code = "";
                  switch(error.status) { 
                    case 401: { 
                      error_i18l_code = "LOGIN.AUTH_ERROR"; 
                      break; 
                    }
                    default: { 
                      error_i18l_code = "LOGIN.AUTH_ERROR"; 
                      break; 
                    } 
                  } 

                  this.translocoService.selectTranslate(['LOGIN_TITLE', error_i18l_code]).subscribe((res: any) => {
                    this.toastr.error(res[1], res[0]);
                  });
                }
            );
    }
  }

  register(): void {
    const registerValues = this.registerForm.value;

    if (registerValues.registerEmail && registerValues.registerPassword) {
      this.authService.registr(registerValues.registerEmail, registerValues.registerPassword)
          .subscribe(
              result => {
                  this.logger.debug("User successfully Registered with Email: " + registerValues.registerEmail + " Password: " + registerValues.registerPassword);
                  this.router.navigateByUrl('/profile');
              },
              error => {
                let error_i18l_code = "";
                switch(error.status) { 
                  case 406: { 
                    error_i18l_code = "REGISTR.ERROR"; 
                    break; 
                  }
                  case 409: { 
                    error_i18l_code = "REGISTR.CONFLICT";
                    break; 
                  }
                  default: { 
                    error_i18l_code = "REGISTR.ERROR"; 
                    break; 
                  } 
                }

                this.translocoService.selectTranslate(['REGISTR_TITLE', error_i18l_code]).subscribe((res: any) => {
                  this.toastr.error(res[1], res[0]);
                });
              }
          );
    }



  }
  

}


