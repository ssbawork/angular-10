import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoggerModule, NgxLoggerLevel } from "ngx-logger";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './parts/home/home.component';
import { MobileService } from './services/mobile.service';
import { WindowService } from './services/ssr/windows.service';
import { DocumentService } from './services/ssr/document.service';
import { CategoriesService } from './services/categories.service';
import { AuthService } from './services/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FooterComponent } from './parts/footer/footer.component';
import { LoginComponent } from './parts/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { FlagComponent } from './parts/flag/flag.component';
import { ShopComponent } from './parts/shop/shop.component';
import { ProductComponent } from './parts/product/product.component';
import { CartComponent } from './parts/cart/cart.component';
import { FaqComponent } from './parts/faq/faq.component';
import { BulkComponent } from './parts/bulk/bulk.component';
import { DeliveryComponent } from './parts/delivery/delivery.component';
import { ContactsComponent } from './parts/contacts/contacts.component';
import { WhoWeAreComponent } from './parts/who-we-are/who-we-are.component';
import { OrderStatusComponent } from './parts/order-status/order-status.component';
import { ThankYouComponent } from './parts/thank-you/thank-you.component';
import { HeaderModule } from './parts/header/header.module';
import { NotFoundComponent } from './parts/not-found/not-found.component';
import { HomeTopProductsComponent } from './parts/home/home-top-products/home-top-products.component';
import { HomeUsefulTipsComponent } from './parts/home/home-useful-tips/home-useful-tips.component';
import { HomeRecommendComponent } from './parts/home/home-recommend/home-recommend.component';
import { HomeBestSellingComponent } from './parts/home/home-best-selling/home-best-selling.component';
import { GoUpComponent } from './parts/go-up/go-up.component';
import { BreadCrumbsComponent } from './parts/bread-crumbs/bread-crumbs.component';
import { ThemeService } from './services/theme.service';
import { ProductService } from './services/product.service';
import { SearchFilterService } from './services/searchFilter.service';
import { ShopSidebarComponent } from './parts/shop/shop-sidebar/shop-sidebar.component';
import { ProductCommentsComponent } from './parts/product/product-comments/product-comments.component';
import { ProductReviewsComponent } from './parts/product/product-reviews/product-reviews.component';
import { ProductQAComponent } from './parts/product/product-qa/product-qa.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollerService } from './services/scroller.service';
import { OrderConfirmationComponent } from './parts/order-confirmation/order-confirmation.component';
import { BlogsComponent } from './parts/blogs/blogs.component';
import { BlogArticleComponent } from './parts/blog-article/blog-article.component';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { TranslocoRootModule } from './transloco/transloco-root.module';
import { ToastrModule } from 'ngx-toastr';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    FlagComponent,
    ShopComponent,
    ProductComponent,
    CartComponent,
    FaqComponent,
    BulkComponent,
    DeliveryComponent,
    ContactsComponent,
    WhoWeAreComponent,
    OrderStatusComponent,
    ThankYouComponent,
    NotFoundComponent,
    HomeTopProductsComponent,
    HomeUsefulTipsComponent,
    HomeRecommendComponent,
    HomeBestSellingComponent,
    GoUpComponent,
    BreadCrumbsComponent,
    ShopSidebarComponent,
    ProductCommentsComponent,
    ProductReviewsComponent,
    ProductQAComponent,
    OrderConfirmationComponent,
    BlogsComponent,
    BlogArticleComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    LoggerModule.forRoot({serverLoggingUrl: '/api/clientlogs', level: NgxLoggerLevel.DEBUG, serverLogLevel: NgxLoggerLevel.ERROR}),
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    HeaderModule,
    BrowserAnimationsModule,
    NgxSliderModule,
    TranslocoRootModule,
    ToastrModule.forRoot({}),
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    LoadingBarModule
  ],
  providers: [
    ThemeService,
    MobileService,
    WindowService,
    DocumentService,
    CategoriesService,
    AuthService,
    SearchFilterService,
    ProductService,
    ScrollerService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }