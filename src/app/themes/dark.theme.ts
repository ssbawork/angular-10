import { Theme } from "./theme.interface";

export const Dark: Theme = {
    name: "Dark",
    properties: {
      "--bg": "#2c2c2c",
      "--label": "#fff",
      "--header": "#323232",
      "--link": "#575757",
      "--order": "#3a3a3a",
      "--status": "#575757",
      "--checkbox": "#474747",
      "--comment": "#7f7f7f",
      "--hover": "#eee",
      "--loading-p": "#676767"
    }
};