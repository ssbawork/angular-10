import { Theme } from "./theme.interface";

export const White: Theme = {
    name: "White",
    properties: {
      "--bg": "#fff",
      "--label": "#000",
      "--header": "#f6f6f6",
      "--link": "#B9B9B9",
      "--order": "#e9e9e9",
      "--status": "#bebebe",
      "--checkbox": "#e3e3e3",
      "--comment": "#f4f4f4",
      "--hover": "#323232",
      "--loading-p": "#ddd"
    }
};