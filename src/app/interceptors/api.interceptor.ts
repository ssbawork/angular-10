import { Injectable } from '@angular/core';
import { LocalStorageService } from '../services/ssr/localStorage.service';
import { environment } from '../../environments/environment';
import { NGXLogger } from "ngx-logger";

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(private localStorageService: LocalStorageService, private logger: NGXLogger) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    const Endpoint = environment.apiUrl + '/api';

    let regexp = new RegExp('^http:\/\/localhost:4200\/assets\/i18n\/[a-z]{2}\.json$');
    if(regexp.test(req.urlWithParams)){
      console.log();
      return next.handle(req);
    }

    const dupReq = req.clone({ url: Endpoint + req.urlWithParams });   
    this.logger.debug("Overwriting API endpoint to " + Endpoint, req)      
    return next.handle(dupReq);
  }

}
