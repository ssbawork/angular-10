import { Injectable } from '@angular/core';
import { LocalStorageService } from '../services/ssr/localStorage.service';
import { NGXLogger } from "ngx-logger";

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private localStorageService: LocalStorageService, private logger: NGXLogger) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const idToken = this.localStorageService.getItem("token");
    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set("Authorization",
          "Bearer " + idToken)
      });
      this.logger.debug("Current Token is " + idToken);
      return next.handle(cloned);
    }
    else {
      this.logger.debug("Token not found");
      return next.handle(req);
    }
  }

}
